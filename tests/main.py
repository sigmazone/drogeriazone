from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.action_chains import ActionChains
from time import sleep
import random
import string


def wait_for_element(attribute, value):
    while True:
        try:
            driver.find_element(attribute, value)
            sleep(0.5)
            break
        except NoSuchElementException:
            pass


def random_string(n):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(n))
    return result_str


def random_date_string():
    year = random.randint(1900, 2022)
    month = random.randint(1, 12)
    if month in [1, 3, 5, 7, 8, 10, 12]:
        day = random.randint(1, 31)
    elif month in [4, 6, 9, 11]:
        day = random.randint(1, 30)
    else:
        day = random.randint(1, 28)

    date_str = str(year) + "-"
    if month < 10:
        date_str += "0"
    date_str += str(month) + "-"
    if day < 10:
        date_str += "0"
    date_str += str(day)
    return date_str


if __name__ == '__main__':
    options = webdriver.ChromeOptions()
    options.add_argument('ignore-certificate-errors')
    driver = webdriver.Chrome(chrome_options=options, service=Service(ChromeDriverManager().install()))
    driver.get("https://localhost:18447/")
    action_chain = ActionChains(driver)

    # Creating new account
    #driver.find_element(By.LINK_TEXT, "Zaloguj się").click()
    driver.find_element(By.CLASS_NAME, "material-icons").click()
    driver.find_element(By.PARTIAL_LINK_TEXT, "Nie masz konta").click()
    sleep(0.5)
    driver.find_element(By.ID, "field-id_gender-" + str(random.randint(1, 2))).click()
    driver.find_element(By.ID, "field-firstname").send_keys(random_string(random.randint(5, 10)))
    driver.find_element(By.ID, "field-lastname").send_keys(random_string(random.randint(5, 10)))
    driver.find_element(By.ID, "field-email").send_keys(random_string(random.randint(5, 10)) + "@gmail.com")
    driver.find_element(By.ID, "field-password").send_keys(random_string(20))
    driver.find_element(By.ID, "field-birthday").send_keys(random_date_string())
    driver.find_element(By.NAME, "customer_privacy").click()
    driver.find_element(By.CLASS_NAME, "form-control-submit").click()
    sleep(0.5)

    # First category selection
    action_chain.move_to_element(driver.find_element(By.ID, "category-3")).perform()
    sleep(0.5)
    driver.find_element(By.ID, "category-6").click()

    # Adding 5 products to cart
    for _ in range(5):
        driver.find_element(By.CLASS_NAME, "product").click()
        for _ in range(random.randint(0, 2)):
            driver.find_element(By.CLASS_NAME, "bootstrap-touchspin-up").click()
        wait_for_element(By.CLASS_NAME, "add-to-cart")
        driver.find_element(By.CLASS_NAME, "add-to-cart").click()
        sleep(0.5)
        driver.refresh()
        sleep(1)

    # Second category selection
    action_chain.move_to_element(driver.find_element(By.ID, "category-3")).perform()
    sleep(0.5)
    driver.find_element(By.ID, "category-4").click()
    
    sleep(3)
    # Adding 5 products to cart
    for _ in range(5):
        driver.find_element(By.CLASS_NAME, "product").click()
        for _ in range(random.randint(0, 2)):
            driver.find_element(By.CLASS_NAME, "bootstrap-touchspin-up").click()
        wait_for_element(By.CLASS_NAME, "add-to-cart")
        driver.find_element(By.CLASS_NAME, "add-to-cart").click()
        sleep(0.5)
        driver.refresh()
        sleep(1)

    # Removing 1 item from cart
    sleep(0.5)
    driver.find_element(By.ID, "_desktop_cart").click()
    wait_for_element(By.CLASS_NAME, "remove-from-cart")
    sleep(0.5)
    driver.find_element(By.CLASS_NAME, "remove-from-cart").click()
    sleep(0.5)

    # Order completion
    driver.find_element(By.CLASS_NAME, "cart-detailed-actions").click()
    sleep(1)
    driver.find_element(By.ID, "field-address1").send_keys(random_string(8) + str(random.randint(1, 9)))
    driver.find_element(By.ID, "field-postcode").send_keys(str(random.randint(10, 99)) + "-" + str(random.randint(100, 999)))
    driver.find_element(By.ID, "field-city").send_keys(random_string(10))
    sleep(3)
    driver.find_element(By.NAME, "confirm-addresses").click()
    wait_for_element(By.NAME, "confirmDeliveryOption")
    # Uncomment this when delivery method selection is implemented
    wait_for_element(By.ID, "delivery_option_4")
    driver.find_element(By.ID, "delivery_option_4").click()
    sleep(1)
    driver.find_element(By.NAME, "confirmDeliveryOption").click()
    wait_for_element(By.ID, "payment-confirmation")
    driver.find_element(By.ID, "payment-option-1-container").click()
    driver.find_element(By.ID, "conditions_to_approve[terms-and-conditions]").click()
    driver.find_element(By.CLASS_NAME, "btn.btn-primary.center-block").click()
    wait_for_element(By.ID, "content-hook_order_confirmation")

    # Order Status check
    driver.find_element(By.CLASS_NAME, "account").click()
    driver.find_element(By.ID, "history-link").click()
    sleep(5)

    # Exit
    driver.quit()
