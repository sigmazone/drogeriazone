#!/usr/bin/python
import random
import csv
import re
import sys
import requests
import json
from bs4 import BeautifulSoup

BASE_URL = "https://perfumy.pl"
URL = "https://perfumy.pl/pol_m_Perfumy-meskie_Wody-perfumowane-meskie-163.html"
categories = []
products = []
combinations = []
EXCLUDED_CATEGORIES = ['Perfumy dla kobiet', 'Zestawy perfum', 'Grupy zapachowe', 'TOP perfumy', 'Oferty specjalne']
def get_product_details(product_details_url):
    prod_page = requests.get(product_details_url)
    prod_soup = BeautifulSoup(prod_page.content, "lxml")

    title = prod_soup.find_all('h1', class_='product_name__name')[0].text

    product_number = None
    try:
        product_number = prod_soup.find_all('div', class_='item_code_info')[0].find_all('strong', class_='iival')[0].contents[0]
    except:
        product_number = random.randint(10000, 1000000)*100

    desc = None
    try:
        desc = ''.join(str(elem) for elem in prod_soup.find('section', id='projector_longdescription').contents)
    except:
        print(f'No description for product: {product_number} ')

    price = None
    try:
        price = float(
            prod_soup.find(id='projector_price_value').text.replace(' ', '').replace('\n', '').split("zł")[0].replace(
                ',', '.'))
    except:
        pass

    max_price = None
    try:
        max_price = float(
            prod_soup.find(id='projector_price_maxprice').text.replace(' ', '').replace('\n', '').split("zł")[
                0].replace(',', '.'))
    except:
        pass

    # product_number = prod_soup.find_all('strong', class_='iival')[0].text
    prod_categories = []
    for category in prod_soup.select('#breadcrumbs>.list_wrapper>ol>li.category>a'):
        if category.contents[0] not in EXCLUDED_CATEGORIES:
            prod_categories.append(category.contents[0])

    versions = []
    sizes = prod_soup.find_all('span', class_='projector_versions__name')
    prices = prod_soup.find_all('span', class_='versions_info__price')
    for i, size in enumerate(sizes):
        versions.append({'size': size.text, 'price': prices[i].text.split('\n')[0].replace(',', '.')})

    brand = 'nieznana'
    params_soup = prod_soup.find_all('div', class_='dictionary__param')
    for param_soup in params_soup:
        if  'Marka' in str(param_soup):
            brand = param_soup.find_all('a', class_='dictionary__value_txt')[0].contents[0]

    # params_soup = prod_soup.find_all("li", {"class": "parameter_element"})
    # params = []
    # for i, param in enumerate(params_soup):
    #     param_name = param.find('strong').text.split(':')[0]
    #     value = ''
    #     try:
    #         value = param.find('a').text
    #     except:
    #         value = param.find('span', {"class": 'n54117_item_b_sub'}).text
    #     params.append({param_name: value})
    # grupa_zapachowa = prod_soup.find_all('a', class_='traits__value')[0].contents[0]
    # tags = []
    # tags_soup = prod_soup.find_all("section", id='projector_dictionary')[0].find_all('a', class_='dictionary__value_txt')
    # for tag in tags_soup:
    #     tags.append(tag.contents[0])
    # tags_soup = prod_soup.find_all("section", id='projector_dictionary')[0].find_all('span', class_='dictionary__value_txt')
    # for tag in tags_soup:
    #     tags.append(tag.contents[0])

    photos_soup = prod_soup.find_all('a', {"class": "photos__link"})
    photos = set()
    for photo in photos_soup:
        photos.add(BASE_URL + photo['href'])
    photos = list(photos)

    return {
        'product_url': product_details_url,
        'product_number': product_number,
        'title': title,
        'brand': brand,
        'categories': prod_categories,
        'description': desc,
        'price': price,
        'max_price': max_price,
        'versions': versions,
        # 'parameters': params,
        'photo_urls': photos
    }


def inner_HTML(html_tag):
    text = ""
    for c in html_tag.contents:
        text += str(c)
    return text


def save_products(products, file_name):
    # Writing to sample.json
    with open(file_name, "w", encoding='utf8') as outfile:
        json.dump(products, outfile, ensure_ascii=False)


def save_data_to_csv(data, file_name):
    # now we will open a file for writing
    output_file = f'{file_name}.csv'
    data_file = open(output_file, 'w', encoding='utf-8', newline="")

    # create the csv writer object
    csv_writer = csv.writer(data_file, delimiter='~')

    # Counter variable used for writing
    # headers to the CSV file
    count = 0

    for data_obj in data:
        if count == 0:

            # Writing headers of CSV file
            header = data_obj.keys()
            csv_writer.writerow(header)
            count += 1

        # Writing data of CSV file
        csv_writer.writerow(data_obj.values())

    data_file.close()


# LOOKS FOR PRODUCTS ON ALL PAGES
def get_product_urls(page_url):
    sub_page = requests.get(page_url)
    sub_soup = BeautifulSoup(sub_page.content, "lxml")

    pagination = sub_soup.select('#paging_setting_top>ul>li>.pagination__link')
    pages = 1
    if len(pagination)>0:
        pages = int(sub_soup.select('#paging_setting_top>ul>li>.pagination__link')[-2].text)

    links = []
    if pages > 4:
        pages = 4

    for page in range(1, pages):
        sub_page = requests.get(f'{page_url}?counter={page}')
        sub_soup = BeautifulSoup(sub_page.content, "lxml")
        curr_page_prods = sub_soup.find(id='search').find_all("div", class_='product')
        for product in curr_page_prods:
            links.append(BASE_URL + product.find_all('a', class_='product__icon')[0]['href'])
    # print(links)
    return links


def get_combinations(product):
    comb = []
    for version in product['versions']:
        v_size = version['size']
        price = product['price']
        price_diff = 0
        if version['price']:
            price = version['price']
            price_diff = float(price) - product['price']
        comb.append(
            {
                'Identyfikator Produktu (ID)': product['product_number'],
                'Atrybut': 'wielkość:select:0',
                'Wartość': f'{v_size}:0',
                'Koszt własny': price,
                'Zależny od stanu magazynowego': 0,
                'Ilość': 200,
                'Wpływ na cenę': price_diff
            }
        )
    return comb


def scrap_category(url: str):
    product_urls = get_product_urls(url)
    subcategories = set()

    for product_url in product_urls:
        product = get_product_details(product_url)
        if "zestaw" in product['title']:
            continue

        products.append(product)
        for combination in get_combinations(product):
            combinations.append(combination)
        for category in product['categories']:
            subcategories.add(category)

    return subcategories


def get_size_from_title(title: str):
    size = 'inna'
    try:
        size = re.findall("\d+ ml", title.lower())[0]
    except:
        pass
    return f'Pojemnosc:{size}'


def product_already_mapped(new_product, mapped_products: []):
    for product in mapped_products:
        if new_product['product_number'] is product['ID']:
            return True
    return False


def products_mapper(products):
    mapped = []

    for product in products:
        wartosc_rabatu = 0
        wartosc_produktu = product['price']
        if product['max_price']:
            wartosc_rabatu = product['max_price'] - product['price']
            wartosc_produktu = product['max_price']

        if not product_already_mapped(product, mapped):
            mapped.append({
                'ID': product['product_number'],
                'Nazwa': product['title'],
                'Marka': product['brand'],
                'Opis': product['description'],
                'Kategorie': ','.join(product['categories']),
                'Ilość': 200,
                'Cecha': get_size_from_title(product['title']),
                'Cena zawiera podatek. (brutto)': wartosc_produktu,
                'Wartość rabatu': wartosc_rabatu,
                'Adresy URL zdjęcia': ','.join(product['photo_urls'])
            })

    return mapped


def categories_mapper(categories):
    mapped = []
    for category in categories['perfumy']:
        cat_name = list(category.keys())[0]
        subcats = list(category.values())[0]
        mapped.append({'Nazwa': cat_name,
                       'Kategoria nadrzędna': ''
                       })
        for subcat in subcats:
            mapped.append({'Nazwa': subcat,
                           'Kategoria nadrzędna': cat_name
                           })
    return mapped


def test_product_scraping():
    # data = get_product_details('https://perfumy.pl/product-pol-10398-Xerjoff-Erba-Pura-woda-perfumowana-100-ml.html')
    f = open('products.json', encoding='utf-8')

    data = json.load(f)

    mappedProducts = products_mapper(data)

    print(mappedProducts)
    save_data_to_csv(mappedProducts, 'produkty')

# def get_products_csv(data):
def scrape_whole_website():
    categories = {
        'perfumy': []
    }
    homepage = requests.get(BASE_URL)
    homepage_soup = BeautifulSoup(homepage.content, "lxml")
    homepage_links = homepage_soup.select('#menu_navbar>ul>li.nav-item>a')

    for link in homepage_links:
        category_link = BASE_URL
        category_name = link.contents[0]
        if 'Perfumy' in str(link):
            print(category_name)
            category_link += link['href']
            sub_categories = scrap_category(category_link)
            categories['perfumy'].append({category_name: sub_categories})


        # links.append({})
        # print(link)
    # save_products(products, 'produkty');
    mapped_products = products_mapper(products)
    mapped_categories = categories_mapper(categories)
    save_data_to_csv(mapped_categories, 'kategorie')
    save_data_to_csv(mapped_products, 'produkty')
    save_data_to_csv(combinations, 'kombinacje')


if __name__ == '__main__':
    sub = scrape_whole_website()
    # print(sub)
    # scrape_whole_website()
    # test_product_scraping()

    # output_file_name = 'products.json'
    # if len(sys.argv) > 1:
    #     output_file_name = sys.argv[1]
    # scrap_website(output_file_name)
