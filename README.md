# DrogeriaZone

Projekt stworzony na potrzeby przedmiotu Biznes Elektroniczny

## Getting started

To run the project you will need a docker. It is also recommended to use Linux as running Prestashop on Windows docker will be extremely slow.


* build the environment - mysql, phpmyadmin and prestashop
```
docker-compose up -d
```
* open localhost:80



## Preserving data / settings

To save your progress: 
1. Open the phpMyAdmin - `localhost:8080`
2. Select the `drogeriazonedb` database
3. Export to file : `drogeriazonedb > Eksport > Eksport`
3. Put exported file into dbinit and remove prev initial script


## Authors
Gesek Marcin,
Wrzosek Michał,
Żylwis Bartosz
