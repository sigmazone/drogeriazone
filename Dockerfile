FROM alpine:3.17 as src
RUN apk update
RUN apk add git
RUN git clone --single-branch https://gitlab.com/sigmazone/drogeriazone.git

FROM prestashop/prestashop:8.0.0

RUN rm -rf ./install

COPY --from=src /drogeriazone/shopsrc .

# RUN chown -R www-data:root ./
RUN chown -R www-data:root .
RUN mkdir -p /var/www/adminpanel
RUN chown -R www-data:root /var/www/adminpanel


RUN a2enmod ssl
COPY --from=src /drogeriazone/ssl/apache-selfsigned.key /etc/ssl/private
COPY --from=src /drogeriazone/ssl/apache-selfsigned.crt /etc/ssl/certs
COPY --from=src /drogeriazone/ssl/127.0.0.1.conf /etc/apache2/sites-available
RUN a2ensite 127.0.0.1.conf

# EXPOSE 80
# EXPOSE 443

